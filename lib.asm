section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error

exit:
    mov rax, 60 ; код системного вызова
    xor rdi, rdi
    syscall


string_length:
    xor rax, rax ; обнуление аккумулятора (= mov rax, 0), в котором будет хранится длина строки
    .loop:
        cmp byte[rdi + rax], 0 ; проверка на то, равен ли текущий символ NUL
        je .end ; если да, то возврат
        inc rax ; если нет, то счётчик кол-ва символов + 1 (это также смещение)
        jmp .loop ; и проверка следующего символа
    .end:
        ret


print_string:
    ; xor rax, rax
    push rdi
    call string_length
    mov rdx, rax
    pop rsi
    mov rax, 1
    mov rdi, 1
    syscall
    ret


print_char:
    ; xor rax, rax
    push rdi ; сохранение символа в стек
    mov rax, 1 ; код syscall'а write
    mov rdi, 1 ; арг 1: дескриптор stdout
    mov rsi, rsp ; арг 2: адрес (указатель) строки (здесь указатель на стек, т.к. символ сохранили туда)
    mov rdx, 1 ; арг 3: кол-во байт (длина строки) (1 символ = 1 байт)
    syscall ; системный вызов
    pop rdi ; возвращение стека в его исходное состояние
    ret


print_newline:
    ; xor rax, rax
    mov rdi, 0xA ; запись символа перевода строки (аргумент функции)
    jmp print_char ; переход к функции, которая принимает символ и выводит его в stdout


print_uint:
    ; xor rax, rax
    mov rax, rdi ; запись в AC переданного числа
    mov rcx, 10 ; основание системы счисления
    mov r8, rsp ; сохранение начального указателя стека
    push 0 ; запись в стек символа конца строки
    .loop:
        xor rdx, rdx ; обнуление DR
        div rcx ; целочисленное деление на 10: целую часть в AC, остаток в DR
        add rdx, 0x30 ; перевод цифры в ASCII код
        dec rsp ; спуск вниз по стеку (в обратную сторону)
        mov byte[rsp], dl ; запись цифры в стек
        cmp rax, 0 ; проверка на то, равен ли текущий символ NUL
        je .end ; если да, то возврат
        jmp .loop ; если нет, то считывание следующей цифры
    .end:
        mov rdi, rsp ; запись указателя на строку (начало строки) (аргумент функции)
        push r8 ; сохранение начального указателя
        call print_string ; вызов функции, которая принимает указатель на строку и выводит её в stdout
        pop r8 ; возвращение стека в его исходное состояние
        mov rsp, r8 ; ; возвращение стека в его исходное состояние
        ret


print_int:
    ; xor rax, rax
    cmp rdi, 0 ; сравнение числа с нулём
    jge print_uint ; если неотрицательное (больше или равно нулю), то вывод его как беззнакового
    push rdi ; если нет, то сохранение в стек
    mov rdi, '-' ; запись символа "минус"
    call print_char ; вывод символа "минус"
    pop rdi ; перемещение числа в DR
    neg rdi ; изменение знака числа (дополнительный код)
    jmp print_uint ; вывод модуля числа
    ret


string_equals:
    xor rax, rax ; очистка нужных регистров
    xor rdx, rdx ; младшие - dl
    xor rcx, rcx ; младшие - cl
    xor r8, r8 ; счётчик
    .loop:
        mov dl, byte[rdi + r8] ; взятие символа из 1-ой строки
        mov cl, byte[rsi + r8] ; взятие символа из 2-ой строки
        cmp dl, cl ; сравнение символов
        jne .not_equals ; если не равны, то возврат 0
        cmp dl, 0 ; проверка на конец строки
        je .equals ; если конец строки, то возврат 1 (все символы совпали)
        inc r8 ; если нет, то счётчик + 1
        jmp .loop ; и проверка следующего символа
    .equals:
        mov rax, 1 ; return 1
        ret
    .not_equals:
        xor rax, rax ; return 0
        ret


read_char:
    xor rax, rax ; mov rax, 0 - код syscall'а read
    xor rdi, rdi ; mov rdi, 0 - дескриптор stdin
    mov rdx, 1 ; длина строки (1 символ)
    push 0 ; выделение памяти под считываемый символ
    mov rsi, rsp ; адрес чтения
    syscall
    pop rax ; перемещение символа в AC и его возврат
    ret 


read_word:
    ; xor rax, rax
    xor rcx, rcx ; счётчик длины
    .loop:
        push rdi ; сохранение адреса начала буфера
        push rsi ; а также размер буфера
        push rcx ; и значение счётчика в стек
        call read_char ; чтение символа
        pop rcx ; возврат значений из стека
        pop rsi
        pop rdi

        cmp rax, 0x20 ; проверка на символ пробела
        je .whitespace ; если да, то переход на .whitespace
    
        cmp rax, 0x9 ; проверка на символ табуляции
        je .whitespace ; если да, то переход на .whitespace
    
        cmp rax, 0xA ; проверка на символ переноса строки
        je .whitespace ; если да, то переход на .whitespace
    
        cmp rax, 0 ; проверка на конец строки
        je .end ; если да, то переход на .end
    
        mov [rdi + rcx], rax ; если обычный символ, то сохранение его в ячейку по адресу
        inc rcx ; счётчик длины + 1
    
        cmp rcx, rsi ; проверка на выход за пределы буфера
        jge .error ; если выход за пределы буфера, то возврат 0
        jmp .loop ; если нет, то считывание следуюшего символа
    .whitespace:
        cmp rcx, 0 ; проверка на начало слова
        je .loop ; если начало слова, то пропуск пробельных символов
        jmp .end ; если нет, то завершение ввода
    .error:
        xor rax, rax ; retrun 0
        xor rdx, rdx
        ret
    .end:
        xor rax, rax ; Обнуление AC (AC = NUL)
        mov [rdi + rcx], rax ; запись NUL в память
        mov rax, rdi ; возврат адреса буфера
        mov rdx, rcx ; возврат длины строки
        ret


parse_uint:
    xor rax, rax ; обнуление AC
    xor rsi, rsi ; счётчик длины
    mov r8, 10 ; основание системы счисления
    .loop:
        movzx rcx, byte[rdi + rsi] ; считывание символа с расширением старших байтов
        
        cmp rcx, 0 ; проверка на конец строки
        je .end ; если конец строки, то переход в .end

        cmp cl, '0' ; проверка на то, что цифра меньше 0 (т.е. не цифра)
        jl .end ; если меньше, то переход в .end

        cmp cl, '9' ; проверка на то, что цифра больше 0 (т.е. не цифра)
        jg .end ; если больше, то переход в .end

        mul r8 ; сдвиг числа в AC влево на 1 разряд
        sub cl, 0x30 ; получение цифры из её кода в ASCII (идёт вычитание кода символа '0')
        add rax, rcx ; запись в младший разряд AC полученную цифру
        inc rsi ; счётчик длины + 1
        jmp .loop ; считывание следующего символа
    .end:
        mov rdx, rsi ; запись в DR длины числа и её возврат
        ret


parse_int:
    xor rax, rax ; обнуление AC
    xor rdx, rdx ; счётчик длины
    cmp byte[rdi], '-' ; проверка на наличие символа "минус"
    je .is_negative ; если есть, то переход на .is_negative
    jmp parse_uint ; если нет, то парсить число как беззнаковое
    .is_negative:
        inc rdi ; смещение адреса начала строки на 1 (чтобы не считать минус)
        call parse_uint ; получение модуля числа
        neg rax ; изменение знака числа (дополонительный код)
        inc rdx ; добавление "минуса" к длине числа
        ret


string_copy: ; args: rdi, rsi, rdx
    xor rax, rax
    xor rcx, rcx ; счётчик длины

    push rdi ; сохранение указателя на строку
    push rsi ; а также указатель на буфер
    push rdx ; и длину буфера в стек
    push rcx ; счётчик длины
    call string_length ; получение длины строки
    pop rcx ; возврат значений из стека
    pop rdx
    pop rsi
    pop rdi

    mov r8, rax ; сохранение длины строки в регистр r8
    cmp rdx, r8 ; проверка на то, что длина буфера меньше длины строки (умещается в буфере или нет)
    jl .error ; если меньше, то возврат 0

    .loop:
        cmp rcx, r8 ; проверка на то, что счётчик длины строки больше её самой (т.е. пройдена вся строка)
        jg .end ; если больше, то возврат длины строки
        mov r9, [rdi + rcx] ; перемещение символа из строки в регистр r9
        mov [rsi + rcx], r9 ; перемещение символа из регистра r9 в буфер
        inc rcx ; счётчик длины + 1
        jmp .loop ; считывание следующего символа
    .error:
        xor rax, rax ; return 0
        ret
    .end:
        mov rax, r8 ; возврат длины строки
        ret

; NEW
print_error:
	push rdi
	call string_length
	mov rdx, rax
    pop rsi
	mov rax, 1
	mov rdi, 2
	syscall
	ret