section .text
%include "lib.inc"
global find_word

find_word:
    xor rax, rax ; AC = 0
    .loop:
        push rdi ; сохранение регистров
        push rsi
        add rsi, 8 ; взятие следующего элемента
        call string_equals ; сравнение строк: AC = 1 если равны, AC = 0 если нет
        pop rsi
        pop rdi
        cmp rax, 1 ; если строки равны
        je .exists ; то переход в .exists
        mov rsi, [rsi] ; иначе переход к след. элементу в словаре
        cmp rsi, 0 ; если элемента нет
        je .not_exists ; то переход в .not_exists
        jmp .loop ; иначе проверка след. элемента
    .exists:
        mov rax, rsi ; return адрес начала вхлждения
        ret
    .not_exists:
        xor rax, rax ; return 0
        ret
