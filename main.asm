section .rodata
%define BUFFER_SIZE 256
%include "words.inc"
GREETING_MESSAGE: db 'Введите ключ: ', 0
BUFFER_OVERFLOW_MESSAGE: db 'Ошибка: переполнение буфера (слишком длинный ключ)', 0
EMPTY_DICTIONARY_MESSAGE: db 'Ошибка: пустой словарь', 0
VALUE_NOT_FOUND_MESSAGE: db 'Ошибка: значение по ключу не найдено', 0
VALUE_FOUND_MESSAGE: db 'Найденное значение по ключу: ', 0

section .text
%include "lib.inc"
%include "dict.inc"
global _start

_start:
    mov rsi, first_element ; помещение в rsi адрес первого элемента словаря
    cmp rsi, 0 ; если адрес равен нулю
    je .empty_dictionary ; то словарь пустой
	
	mov rdi, GREETING_MESSAGE ; вывод приветсвенного сообщения
	call print_string

	sub rsp, BUFFER_SIZE ; задание адреса начала буфера
	mov rdi, rsp
	mov rsi, BUFFER_SIZE ; задание размера буфера
	call read_word ; считывание слова (ключа)

	cmp rax, 0 ; если переполнение буфера (не поместилось в буфер)
	je .buffer_overflow ; то вывод соответствующего сообщения

	mov rsi, first_element ; адрес первого элемента словаря
	mov rdi, rax ; введённое слово (ключ)
	call find_word ; поиск по значения по ключу в словаре

	cmp rax, 0 ; если не найден
	je .value_not_found ; то вывод соответствующего сообщения

	; в ином случае:
	add rax, 8 ; смещение к адресу начала введённого слова (ключа)
	push rax ; сохранение адреса ключа
	mov rdi, VALUE_FOUND_MESSAGE
	call print_string ; вывод сообщения о том, что значение найдено
	pop rdi ; в rdi - адрес ключа
	call string_length ; вичисление длины ключа
	inc rax ; смещение на 1 (нуль-терминатор)
	add rdi, rax ; rdi = адрес_ключа + длина_ключа + нуль_терминатор

	call print_string ; вывод значения по ключу
	jmp .exit ; выход
	.empty_dictionary:
		mov rdi, EMPTY_DICTIONARY_MESSAGE
		call print_error
		jmp .exit
	.buffer_overflow:
		mov rdi, BUFFER_OVERFLOW_MESSAGE
		call print_error
		add rsp, BUFFER_SIZE ; возвращение стека в исходное состояние
		jmp .exit
	.value_not_found:
		mov rdi, VALUE_NOT_FOUND_MESSAGE
		call print_error
		add rsp, BUFFER_SIZE ; возвращение стека в исходное состояние
		jmp .exit
	.exit:
		call print_newline
		call exit
