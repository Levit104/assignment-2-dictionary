ASM=nasm
ASMFLAGS=-f elf64 -o
LD=ld
LDFLAGS=-o
FILES=main.o lib.o dict.o

program: $(FILES)
	$(LD) $(LDFLAGS) $@ $+
	
%.o: %.asm
	$(ASM) $(ASMFLAGS) $@ $<
	
%.asm: %.inc
	touch $@
	
main.asm: words.inc lib.inc colon.inc
	touch $@
	
.PHONY: clean
clean:
	$(RM) *.o
	$(RM) program
