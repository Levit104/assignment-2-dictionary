%define first_element 0 ; задание начального элемента

; арг 1: ключ, арг 2: имя метки
%macro colon 2 
%%next_element: dq first_element ; ссылка на следующий элемент
%define first_element %%next_element ; изменение первого элемента на текущий
db %1, 0 ; сохранение ключа
%2: ; создание метки для след. значения
%endmacro
